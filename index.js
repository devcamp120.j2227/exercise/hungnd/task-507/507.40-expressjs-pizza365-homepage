// Import thư viện expressjs tương đương import express from "express";
const express = require("express");
// Import thư viện path
const path = require("path");
// Khởi tạo 1 app express
const app = express();
// khai báo cổng chạy project
const port = 8000;
//Callback function là 1 function đóng vài trò là tham số của 1 function khác, nó sẽ thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/Pizza 365 v1.9 Menu.html"));
});

app.use(express.static(__dirname + "/views"));
app.listen(port, () => {
    console.log("App listening on port: ", port);
});